#Program for sum of the first n numbers 
#3-> 3+2+1
"""print ("Input Number")

firstN = int(input())
sum = 0
for i in range (firstN+1): 
    sum += i 
print (str(sum))"""

#Program for sum of first n Even Numbers 
# 3 => 2+4+6
"""print ("Input a Number")
firstN = int(input())
sum = 0 
for i in range (1,firstN*2+1):
    if i%2 == 0:  
        sum += i
print (str(sum))"""

#Program for sum of first n Odd Numbers 
#3 => 1+3+5 = 9
"""print ("Input number")
firstN = int(input())
sum = 0
for i in range (1,firstN*2):
    if i%2 !=0:
        sum +=i
print ((str(sum)))"""

#program to compute x^30 + x^29....+x^0 
#geometric sum: (1-3^30)/(1-3)
"""print ("Input a Number")
base = int(input())
sum = 0
for exponent in range (30,0,-1): 
    sum += int(base**exponent)
finalsum = sum + 1 
print (str(finalsum))"""
#Shorter Program for the Exponential Sum 
"""print ("Input a Number")
n = int(input())
Sum = int((1-n**31)/(1-n))
print (str(Sum))"""

#Shorter Program for Calculating sum of first N terms
"""print ("Input Number")
n = int(input())
sum = n*(n+1) // 2
print (str(sum))
"""
#Shorter Program for Calculating Sum of First N even Terms
"""
print ("Input number")
n = int(input())
sum = int(n /2 * (2 + 2*n))
print (str(sum))
"""
#Shorter Program for sum of all odd numbers 
"""
print ("Input Number")
n = int(input())
sum = int(n/2 * (1 + (2*n-1)))
print (str(sum)
"""






