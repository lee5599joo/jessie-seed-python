#Homework for 7/27/19
#For all integers n, the following process will terminate
# if n = 1: stop
# if n is even, divide by 2
# if n is odd, multiply by 3 and add 1
# User inputs a number n
# Given n, return number of steps before terminating
# Keep track of the maximal value during this process and print that as well
"""def collatz(x):
    result = 0
    while x !=1:
        if x % 2 == 0:
            x = x / 2
            
        else:
            x = x * 3 + 1
        result = result + 1
       return result """

# FIBONACCI NUMBERS
# fib(n)
# Given n, output the nth Fibonacci number
# ex. n = 0: 0, n = 1: 1, n = 2: 1, n = 3: 2
"""
n = int(input())
def Fibonacci(n):
    if n == 1 or n == 2:
        return 1
    else: return Fibonacci(n-1) + Fibonacci(n-2)
    print (Fibonacci(n))

"""
#BRUTE FORCE GCD:
# gcd(a, b)
# Given two numbers, compute their greatest common divisor
# Idea: think about the largest possible GCD and work your way down and check
# if your candidate divides both.

"""def gcd_(a, b):
gcd = 1

for i in range (2, min(a, b) + 1):
    while (a % i == 0 and b % i == 0):
        a = a // i
        b = b // i
        gcd = gcd * i
return gcd 
"""
 #LUCAS SEQUENCE:
# lucas(p, q, n)
# Lucas sequence is a generalization of Fibonacci sequence
# L_0 = 0
# L_1 = 1
# L_n = p * L_{n-1} - q * L_{n-2}
# if p = 1 and q = -1, then this is just Fibonacci
# Write a function that takes in p,q,n and outputs nth Lucas number
#MAX GAP:
# max_gap(arr)
# Given an array of integers, find the largest "gap", meaning the
# largest absolute difference in value between any two consecutive elements.
# ex. [5,8,1,2,-5,3,9] => 8 (between -5 and 3)
"""
def maxgap (a):
    x = len(a)
    list = []
    for i in range (x-1):
        for j in range (i+1, x):
            if a[j] > a[i]:
                gap = a[j] - a[i]
                arr.append(gap)
    return (max(arr))
"""

            
        